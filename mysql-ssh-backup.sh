#!/bin/bash
source /root/scripts/variables-backup.sh

$SSHBIN -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" $SSH 'mkdir -p '$BACKUPDIR'/'$DATA''
rm -rf $OUTPUT
echo > $BACKUPLOG
mkdir $OUTPUT

for db in $($MYSQLBIN -e "SHOW DATABASES;" | tr -d "| " | grep -v Database); do
    if [[ "$db" != "information_schema" ]] && [[ "$db" != _* ]] ; then
        start=$(date +%s)
        if $($MYSQLDUMPBIN  --single-transaction --force --opt --routines $db | gzip > $OUTPUT/$db.sql.gz); then
                STATUS="OK"
                else
                STATUS="FAIL"
		        touch $OUTPUT/backup.failed
        fi
        $SCPBIN -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" $OUTPUT/$db.sql.gz $SSH:$BACKUPDIR/$DATA/$db.sql.gz >> /dev/null 2>&1
        stop=$(date +%s)
        echo "Dumping database: $db '$((stop-start))s' : $STATUS" >> $BACKUPLOG
        rm -f $OUTPUT/$db.sql.gz
    fi
done

#send message
backupsize=$($SSHBIN -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" $SSH 'du -sh '$BACKUPDIR'/'$DATA'')
sed -i '1 i\ Backup size: '"$backupsize"'' $BACKUPLOG
if [ -f "$OUTPUT/backup.failed" ]; then
    subject="MySQL Backup - '$HOST' - FAIL" message=$($php -r 'echo json_encode(file_get_contents("/tmp/mysql-tmp.log"));') send_mail
    else
    subject="MySQL Backup - '$HOST' - OK" message=$($php -r 'echo json_encode(file_get_contents("/tmp/mysql-tmp.log"));') send_mail
fi

#fix perms
$SSHBIN -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" $SSH 'find '$BACKUPDIR' -type d -print0 |xargs -0 chmod 700'
$SSHBIN -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" $SSH 'find '$BACKUPDIR' -type f -print0 |xargs -0 chmod 600'

#clear old backups
$SSHBIN -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" $SSH 'find '$BACKUPDIR' -mtime +'$BACKUPTIME' -type d -print0 |xargs -0 rm -rf'

#run custom script
if [ -f "/root/scripts/mysql-custom.sh" ]
then
bash /root/scripts/mysql-custom.sh $1
fi

if [ -f "/root/scripts/backup-list.sh" ]
then
bash /root/scripts/backup-list.sh $1
fi