#!/bin/bash
CURL=$(which curl)
UFW=$(which ufw)

for d in $($CURL -s https://bitbucket.org/vibiznes/vipower/raw/master/netgraf/netgraf-IP.list | awk '{print $1}' | grep -v '^#')
do
$UFW allow from $d
done

for d in $($CURL -s https://bitbucket.org/vibiznes/vipower/raw/master/netgraf/netgraf-port.list | awk '{print $1}' | grep -v '^#')
do
$UFW allow $d
done
