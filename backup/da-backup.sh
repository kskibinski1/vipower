#!/bin/bash

DA_TASKQUEUE='/usr/local/directadmin/data/task.queue'
DA_PAYLOAD='action=backup&append%5Fto%5Fpath=nothing&database%5Fdata%5Faware=yes&email%5Fdata%5Faware=yes&local%5Fpath=%2Fhome%2Fadmin%2Fnodea%5Fbackups&option%30=autoresponder&option%31=database&option%32=email&option%33=emailsettings&option%34=forwarder&option%35=ftp&option%36=ftpsettings&option%37=list&option%38=subdomain&option%39=vacation&owner=admin&trash%5Faware=yes&type=admin&value=multiple&what=select&when=now&where=local&who=all'
DA_BACKUP_LOG='/dev/null'

source /root/scripts/variables-backup.sh
mkdir -p /home/admin/nodea_backups/
rm -rf /home/admin/nodea_backups/
mkdir -p /home/admin/nodea_backups/
chown admin:admin -R /home/admin/nodea_backups
echo $DA_PAYLOAD >> $DA_TASKQUEUE
/usr/local/directadmin/dataskq >> $DA_BACKUP_LOG