### Simple script to put our SSH key to client servers ###

* wget -q -O - https://bitbucket.org/vibiznes/vipower/raw/master/put-twojadminninja-key.sh | bash
* curl https://bitbucket.org/vibiznes/vipower/raw/master/put-twojadminninja-key.sh | bash

### Polish Lang for DirectAdmin

## Requirments: unzip ##
* wget https://bitbucket.org/vibiznes/vipower/raw/master/DirectAdmin/directadmin-pl.sh
* bash directadmin-pl.sh

### BACKUP
* curl https://bitbucket.org/vibiznes/vipower/raw/master/backup-install.sh | bash
* wget -q -O - https://bitbucket.org/vibiznes/vipower/raw/master/backup-install.sh |bash

### ZABBIX
* wget https://bitbucket.org/vibiznes/zabbix/raw/master/zabbix_deploy.sh -O /root/scripts/zabbix_deploy.sh