#!/bin/bash
source /root/scripts/variables-backup.sh

#borg
borg list $REPOSITORY | awk '{print $1}' > /var/log/borg-backup.log

#sql
ssh $SSH 'du -sh mysql/*' > /var/log/sql-backup.log
ssh $SSH 'du -sh postgres/*' >> /var/log/sql-backup.log