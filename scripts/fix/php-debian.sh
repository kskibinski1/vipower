#!/bin/bash

apt-get install apt-transport-https lsb-release ca-certificates -y
wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list
apt-get update
#PHP5.6
apt-get install php5.6 php5.6-cgi php5.6-cli php5.6-common php5.6-curl php5.6-fpm php5.6-gd php5.6-imap php5.6-intl php5.6-json php5.6-mysql php5.6-pspell php5.6-readline php5.6-recode php5.6-sqlite3 php5.6-tidy php5.6-xmlrpc php5.6-xsl php5.6-mbstring php5.6-bcmath php5.6-soap php5.6-redis php5.6-zip php5.6-imagick php5.6-mcrypt php5.6-memcached php5.6-memcache -y
#PHP7.0
apt-get install php7.0 php7.0-cgi php7.0-cli php7.0-common php7.0-curl php7.0-fpm php7.0-gd php7.0-imap php7.0-intl php7.0-json php7.0-mysql php7.0-pspell php7.0-readline php7.0-recode php7.0-sqlite3 php7.0-tidy php7.0-xmlrpc php7.0-xsl php7.0-mbstring php7.0-bcmath php7.0-soap php7.0-redis php7.0-zip php7.0-imagick php7.0-mcrypt php7.0-memcached php7.0-memcache -y
#PHP7.1
apt-get install php7.1 php7.1-cgi php7.1-cli php7.1-common php7.1-curl php7.1-fpm php7.1-gd php7.1-imap php7.1-intl php7.1-json php7.1-mysql php7.1-pspell php7.1-readline php7.1-recode php7.1-sqlite3 php7.1-tidy php7.1-xmlrpc php7.1-xsl php7.1-mbstring php7.1-bcmath php7.1-soap php7.1-redis php7.1-zip php7.1-imagick php7.1-mcrypt php7.1-memcached php7.1-memcache -y
#PHP7.2
apt-get install php7.2 php7.2-cgi php7.2-cli php7.2-common php7.2-curl php7.2-fpm php7.2-gd php7.2-imap php7.2-intl php7.2-json php7.2-mysql php7.2-pspell php7.2-readline php7.2-recode php7.2-sqlite3 php7.2-tidy php7.2-xmlrpc php7.2-xsl php7.2-mbstring php7.2-bcmath php7.2-soap php7.2-redis php7.2-zip php7.2-imagick php7.2-mcrypt php7.2-memcached php7.2-memcache -y
#PHP7.3
apt-get install php7.3 php7.3-cgi php7.3-cli php7.3-common php7.3-curl php7.3-fpm php7.3-gd php7.3-imap php7.3-intl php7.3-json php7.3-mysql php7.3-pspell php7.3-readline php7.3-recode php7.3-sqlite3 php7.3-tidy php7.3-xmlrpc php7.3-xsl php7.3-mbstring php7.3-bcmath php7.3-soap php7.3-redis php7.3-zip php7.3-imagick php7.3-mcrypt php7.3-memcached php7.3-memcache -y
#PHP7.4
apt-get install php7.4 php7.4-cgi php7.4-cli php7.4-common php7.4-curl php7.4-fpm php7.4-gd php7.4-imap php7.4-intl php7.4-json php7.4-mysql php7.4-pspell php7.4-readline php7.4-sqlite3 php7.4-tidy php7.4-xmlrpc php7.4-xsl php7.4-mbstring php7.4-bcmath php7.4-soap php7.4-redis php7.4-zip php7.4-imagick php7.4-mcrypt php7.4-memcached php7.4-memcache -y
#PHP8.0
apt-get install php8.0 php8.0-cgi php8.0-cli php8.0-common php8.0-curl php8.0-fpm php8.0-gd php8.0-imap php8.0-intl php8.0-mysql php8.0-pspell php8.0-readline php8.0-sqlite3 php8.0-tidy php8.0-xsl php8.0-mbstring php8.0-bcmath php8.0-soap php8.0-redis php8.0-zip php8.0-imagick php8.0-mcrypt php8.0-memcached php8.0-memcache -y
