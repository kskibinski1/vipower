#!/bin/bash

enhanced_pl(){
rm -rf /tmp/directadmin
mkdir /tmp/directadmin
cd /tmp/directadmin
wget https://github.com/regdos/directadmin-enhanced-pl/archive/master.zip -O directadmin.zip
unzip ./directadmin.zip
cd ./directadmin-enhanced-pl-master/
rm -rf /usr/local/directadmin/data/skins/enhanced/lang/pl/
cp -Rn ./pl /usr/local/directadmin/data/skins/enhanced/lang/
cp -Rn ./templates/* /usr/local/directadmin/data/templates/
cd /root
sed -i '/language/d' /usr/local/directadmin/conf/directadmin.conf
echo "language=pl" >> /usr/local/directadmin/conf/directadmin.conf
for u in $(ls /usr/local/directadmin/data/users/)
do
sed -i '/language/d' /usr/local/directadmin/data/users/$u/user.conf
echo "language=pl" >> /usr/local/directadmin/data/users/$u/user.conf
done
}

directadmin_restart(){
service directadmin restart
}

evolution_pl(){
rm -rf /tmp/directadmin
mkdir /tmp/directadmin
cd /tmp/directadmin
wget https://github.com/regdos/DirectAdmin-Evolution-PL/archive/master.zip -O directadmin.zip
unzip ./directadmin.zip
cd ./DirectAdmin-Evolution-PL-master
cp ./pl.po /usr/local/directadmin/data/skins/evolution/lang/
cp ./login-pl.po /usr/local/directadmin/data/skins/evolution/lang/
cp -a ./pl/ /usr/local/directadmin/data/skins/evolution/lang/
cp -a ./templates/custom/ /usr/local/directadmin/data/templates/
cp -a ./templates/email_pass_change/ /usr/local/directadmin/data/templates/
rm /usr/local/directadmin/data/templates/custom/login.html
sed -i '/docsroot/d' /usr/local/directadmin/conf/directadmin.conf
echo "docsroot=./data/skins/evolution" >> /usr/local/directadmin/conf/directadmin.conf
}

enhanced_pl
evolution_pl
directadmin_restart

echo Spolszczenie panelu DirectAdmin zostało zainstalowane.
echo Domyslny jezyk polski
echo Kazdy uzytkownik posiada ustawiony jezyk polski.