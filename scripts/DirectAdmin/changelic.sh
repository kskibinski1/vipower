#!/bin/bash
#*--------------------------------------------------------------------*
#-----------* ViBiznes DirectAdmin Change License script *-------------
#*--------------------------------------------------------------------*
#
#ViBiznes DirectAdmin Change License script
#changelic
#
#This script is a part of ViBiznes
#software licenced under The BSD 3-Clause License.
#
#General BSD license.
#
#Copyright (c) 2016, ViBiznes
#f.stolarski@vibiznes.pl, m.adach@vipower.pl
#
#All rights reserved.
#
#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:
#    * Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#    * Neither the name of the copyright holder nor the
#      names of its contributors may be used to endorse or promote products
#      derived from this software without specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
#ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
#ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#*-------------------------------------*
IP_ADDRESS_CHECK_URL="https://vibiznes.pl/ip_address.php"
DIRECT_ADMIN_PATH="/usr/local/directadmin"
SYSTEMCTL=$(which systemctl)
MYSQLADMIN=$(which mysqladmin)
CURRENT_DIR=$(pwd)
#*-------------------------------------*
VERSION="1.1"

critical_error() {
  echo -e "Critical error! Aborting."
  exit 1
}

genpasswd() {
	local l=$1
       	[ "$l" == "" ] && l=32
      	${TR} -dc A-Za-z0-9_ < /dev/urandom | head -c ${l} | xargs
}

check_distro() {
  if [ -f /etc/lsb-release ]; then
      . /etc/lsb-release
      DISTRO="${DISTRIB_ID}"
      VER="${DISTRIB_RELEASE}"
      if [ "$DISTRO" == "Ubuntu" ]; then
        VER=$(echo $VER | ${TR} -d '.')
        case $VER in
          1004|1010|1104|1110|1204|1210|1304|1310|1404|1410|1504|1510)
            INIT="UPSTART"
          ;;
          1604|1610|1704|1710|1804|1810)
            INIT="SYSTEMD"
          ;;
        esac
        WWW_SERVER_NAME="apache2"
      fi
  elif [ -f /etc/debian_version ]; then
      DISTRO="Debian"
      VER=$(cat /etc/debian_version)
      VER=$(echo $VER | tr -d '.')
      if [ "${VER}" -gt "79" ]; then
        INIT="SYSTEMD"
      else
        INIT="SYSV"
      fi
      WWW_SERVER_NAME="apache2"
  elif [ -f /etc/redhat-release ]; then
      DISTRO="Red Hat"
      VER=$(cat /etc/redhat-release | ${SED} 's/[^0-9]*//g')
      if [ "${VER}" -gt "69" ]; then
        INIT="SYSTEMD"
      else
        INIT="SYSV"
      fi
      WWW_SERVER_NAME="httpd"
  else
      DISTRO=$(uname -s)
      VER=$(uname -r)
      if [ -f "${SYSTEMCTL}" ]; then
        INIT="SYSTEMD"
      else
        INIT="SYSV"
      fi
  fi
}

check_sed() {
  if [ $(which sed) ]; then
    SED=$(which sed)
  else
    echo -e "Could not find 'sed' binary!"
    critical_error
  fi
}

check_tr() {
  if [ $(which tr) ]; then
    TR=$(which tr)
  else
    echo -e "Could not find 'tr' binary!"
    critical_error
  fi
}

check_curl() {
  if [ $(which curl) ]; then
    CURL=$(which curl)
  else
    echo -e "Could not find 'curl' binary!"
    critical_error
  fi
}

check_da() {
  if [ ! -d ${DIRECT_ADMIN_PATH} ]; then
    echo -e "Could not find installed DirectAdmin!"
    critical_error
  fi
}

check_root() {
  RUN_UID=$(id -u)
  if [ "$RUN_UID" -ne 0 ]; then
    log "$0 requires to be run as root!"
    critical_error
  fi
}

check_www_server() {
  if [ $(which httpd) ]; then
    WWW_SERVER="httpd"
  elif [ $(which apache2) ]; then
    WWW_SERVER="apache2"
  elif [ $(which nginx) ]; then
    WWW_SERVER="nginx"
  else
    echo -e "Could not find WWW server!"
    critical_error
  fi
}

check_ftp_server() {
  if [ $(which proftpd) ]; then
    FTP_SERVER="proftpd"
  elif [ $(which pure-ftpd) ]; then
    FTP_SERVER="pure-ftpd"
  elif [ $(which vsftpd) ]; then
    FTP_SERVER="vsftpd"
  else
    echo -e "Could not find FTP server!"
    critical_error
  fi
}

service_manager() {
  case ${INIT} in
    SYSV|UPSTART)
      /etc/init.d/${1} ${@:2}
    ;;
    SYSTEMD)
      ${SYSTEMCTL} ${@:2} ${1}
    ;;
    *)
      critical_error
    ;;
  esac
}

ask_cid() {
  echo -n "DirectAdmin license please enter your Client ID: ";
  read CID;
}

ask_lid() {
  echo -n "DirectAdmin license please enter your License ID: ";
  read LID;
}

get_ip() {
  NEW_IP_ADDRESS=$(${CURL} -s ${IP_ADDRESS_CHECK_URL})
  if [[ ! "${NEW_IP_ADDRESS}" ]]; then
    echo -n "Please enter your server IP: ";
    read NEW_IP_ADDRESS;
  fi
}

pre_print() {
  echo -e "=========================================================================="
  echo -e " ViBiznes (R) DirectAdmin Change License script - version: ${VERSION}     "
  echo -e "=========================================================================="
}

post_print() {
  echo -e "======================================================================="
  echo -e "Rest of your settings can be changed from DirectAdmin Panel login with:"
  echo -e "======================================================================="
  echo -e "DirectAdmin URL: http://${NEW_IP_ADDRESS}:2222"
  echo -e "DirectAdmin username: admin"
  echo -e "DirectAdmin password: ${NEW_ADMIN_PASSWORD}"
}

start() {
  # Clear screen
  clear
  # Pre checks
  check_root
  check_tr
  check_sed
  check_curl
  check_da
  check_distro
  check_www_server
  check_ftp_server
  # Go into DirectAdmin scripts directory
  cd /usr/local/directadmin/scripts
  # Pre print
  pre_print
  # Ask everything
  ask_cid
  ask_lid
  get_ip
  # Change licence IP address
  echo -e ""
  echo -e "Please wait while changing DirectAdmin license..."
  echo -e ""
  sh getLicense.sh ${CID} ${LID} ${NEW_IP_ADDRESS}
  service_manager directadmin restart
  # Swap IP address
  OLD_IP_ADDRESS=$(${SED} -e 1b -e '$!d' /usr/local/directadmin/data/admin/ip.list)
  sh ipswap.sh ${OLD_IP_ADDRESS} ${NEW_IP_ADDRESS}
  OLD_SETUP_IP_ADDRESS=$(cat /usr/local/directadmin/scripts/setup.txt |grep ip= |cut -d = -f 2)
  ${SED} -i 's/'${OLD_SETUP_IP_ADDRESS}'/'${NEW_IP_ADDRESS}'/g' /usr/local/directadmin/scripts/setup.txt
  echo -e "action=cache&value=showallusers" >> /usr/local/directadmin/data/task.queue
  # Change MySQL passwords
  OLD_MYSQL_PASSWORD=$(cat /usr/local/directadmin/conf/mysql.conf |grep passwd | cut -d = -f 2)
  OLD_MYSQL_ROOT_PASSWORD=$(cat /usr/local/directadmin/scripts/setup.txt |grep mysql= | cut -d = -f 2)
  NEW_MYSQL_PASSWORD=$(genpasswd)
  NEW_MYSQL_ROOT_PASSWORD=$(genpasswd)
  ${MYSQLADMIN} -uda_admin -p''${OLD_MYSQL_PASSWORD}'' password ''${NEW_MYSQL_PASSWORD}''
  ${MYSQLADMIN} -uroot -p''${OLD_MYSQL_ROOT_PASSWORD}'' password ''${NEW_MYSQL_ROOT_PASSWORD}''
  ${SED} -i 's/'${OLD_MYSQL_PASSWORD}'/'${NEW_MYSQL_PASSWORD}'/g' /usr/local/directadmin/conf/mysql.conf
  ${SED} -i 's/'${OLD_MYSQL_ROOT_PASSWORD}'/'${NEW_MYSQL_ROOT_PASSWORD}'/g' /usr/local/directadmin/scripts/setup.txt
  # Change admin user password
  NEW_ADMIN_PASSWORD=$(genpasswd)
  OLD_ADMIN_PASSWORD=$(cat /usr/local/directadmin/scripts/setup.txt |grep adminpass |cut -d = -f 2)
  $(echo admin:${NEW_ADMIN_PASSWORD} | chpasswd)
  ${SED} -i 's/'${OLD_ADMIN_PASSWORD}'/'${NEW_ADMIN_PASSWORD}'/g' /usr/local/directadmin/scripts/setup.txt
  # Restart services
  service_manager directadmin restart
  service_manager ${WWW_SERVER} restart
  service_manager ${FTP_SERVER} restart
  service_manager exim restart
  service_manager dovecot restart
  #service_manager da-popb4smtp restart
  # Back to previous directory
  cd ${CURRENT_DIR}
  # Clear history
  history -c
  # Post print
  post_print
  RET_VAL="0"
}

start
exit ${RET_VAL}
